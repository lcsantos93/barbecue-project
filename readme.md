# Trinca - Test Web Full Stack

This project is a technical evaluation for the position of Full Stack web Developer for the company Trinca.

### Installation

Install the dependencies and devDependencies and start the server.

```sh
$ cd app
$ npm install -d
$ npm start
```
## Database

For running database.



## Build With   

* [NodeJS](https://nodejs.org/en/) - JavaScript runtime.
* [nodemon](https://nodemon.io/) - Monitor for changes in source.
* [MongoDb](https://www.mongodb.com/) - Document database.
* [mongoose](http://mongoosejs.com/) - Object Modeling for node.js.
* [Express](http://expressjs.com/) - Framework for node.js.
* [Swig](http://node-swig.github.io/swig-templates/) - JavaScript Template Engine.
* [Npm](https://www.npmjs.com/) - The package manager used.
* [less](http://lesscss.org/) - CSS pre-processor.
* [Grunt](https://gruntjs.com/) - The JavaScript Task Runner used.

## Author

Lucas Cardoso Santos - Full Stack Web Developer.